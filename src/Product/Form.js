import React, { Component } from 'react';

import axios from 'axios'

class ProductForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: "",
      description: "",
      price: 0
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  handleInputChange(event) {
    let target = event.target;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    let name = target.name;

    this.setState({
      [name]: value
    });
  }

  submitForm(event) {
    event.preventDefault();

    axios.post('http://localhost:3001/api/v1/products', {
      product: this.state
    })
    .then(response => {
      this.props.callback({
        id: response.data.location.id,
        name: response.data.location.name,
        price: response.data.location.price
      })

      this.setState({
        name: "",
        description: "",
        price: 0
      });
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div className="row">
        <div className="col s6">
          <form>
            <div className="row">
              <div className="input-field col s12">
                <input type="text" name="name" value={this.state.name} onChange={this.handleInputChange}/>
                <label htmlFor="name">Name</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s12">
                <textarea className="materialize-textarea" name="description" rows="6" value={this.state.description} onChange={this.handleInputChange}/>
                <label htmlFor="description">Description</label>
              </div>
            </div>
            <label>Price:</label>
            <br/>
            <input type="number" name="price" value={this.state.price} onChange={this.handleInputChange}/>
            <br/>
            <button className="btn waves-effect waves-light" type="submit" onClick={this.submitForm}>
              Save
              <i className="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default ProductForm;
