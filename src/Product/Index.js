import React, { Component } from 'react';
import { Link } from "react-router-dom";

import axios from 'axios'

import ProductForm from './Form';

class ProductIndex extends Component {
  constructor(props) {
    super(props)

    this.state = {
      products: [],
      showAddProduct: false
    }

    this.showForm = this.showForm.bind(this);
    this.addProduct = this.addProduct.bind(this);
    this.removeProduct = this.removeProduct.bind(this);
  }

  componentDidMount() {
    axios.get('http://localhost:3001/api/v1/products')
    .then(response => {
      this.setState({products: response.data})
    })
    .catch(error => console.log(error))
  }

  showForm() {
    this.setState(state => ({
      showAddProduct: !state.showAddProduct
    }));
  }

  addProduct(product) {
    let oldProducts = this.state.products;
    oldProducts.push(product);
    this.setState({products: oldProducts});
  }

  removeProduct(productId) {
    let oldProducts = this.state.products;

    let ids = oldProducts.map(product => {
      return product.id
    });

    let index = ids.indexOf(productId);

    oldProducts.splice(index, 1);
    this.setState({products: oldProducts});
  }

  render() {
    return (
      <div className="row">
        <div className="col s12">
          <h2>
            Products
          </h2>
          <a className="waves-effect waves-light btn" onClick={this.showForm}>
            <i className="material-icons left">add</i>
            Add Product
          </a>
          <br/><br/>
          { this.state.showAddProduct && <ProductForm callback={this.addProduct}/> }
          <table className="striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.state.products.map((product) => {
                return (
                  <tr key={product.id}>
                    <td>{ product.name }</td>
                    <td>{ product.price }</td>
                    <td>
                      <Link to={`/products/${product.id}`}>View</Link>
                      <button onClick={this.removeProduct.bind(null, product.id)}>
                        Remove
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ProductIndex;
