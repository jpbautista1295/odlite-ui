import React, { Component } from 'react';
import axios from 'axios'

class ProductShow extends Component {
  constructor(props) {
    super(props)

    this.state = {
      product: {}
    }
  }

  componentDidMount() {
    axios.get(`http://localhost:3001/api/v1/products/${this.props.match.params.id}`)
    .then(response => {
      this.setState({product: response.data})
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        <h1>Product Details</h1>
        <table>
          <tbody>
            <tr>
              <th>ID</th>
              <td>{this.state.product.id}</td>
            </tr>
            <tr>
              <th>Name</th>
              <td>{this.state.product.name}</td>
            </tr>
            <tr>
              <th>Description</th>
              <td>{this.state.product.description}</td>
            </tr>
            <tr>
              <th>Price</th>
              <td>{this.state.product.price}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default ProductShow;
