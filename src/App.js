import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Helmet } from 'react-helmet';

import Navbar from './Navbar';
import Product from './Product';

class App extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>ODLite</title>
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
        </Helmet>
        <Router>
          <div>
            <Navbar/>
            <Product/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
