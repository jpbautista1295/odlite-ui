import React from 'react';
import ReactDOM from 'react-dom';

import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';

import registerServiceWorker from './registerServiceWorker';

import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
