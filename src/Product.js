import React, { Component } from 'react';
import { Route } from "react-router-dom";

import ProductIndex from './Product/Index';
import ProductShow from './Product/Show';

class Product extends Component {
  render() {
    return (
      <div className="container">
        <Route exact path="/products" component={ProductIndex}/>
        <Route exact path="/products/:id(\d+)" component={ProductShow}/>
      </div>
    );
  }
}

export default Product;
