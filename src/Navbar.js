import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => (
  <nav className="light-green darken-1">
    <div className="nav-wrapper">
      <a href="#" className="brand-logo right">ODLite</a>
      <ul id="nav-mobile" className="left hide-on-med-and-down">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/products">Products</Link>
        </li>
      </ul>
    </div>
  </nav>
);

export default Navbar;
